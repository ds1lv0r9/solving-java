package com.totallyvaliddomain.solvingjava.homework.h01.a01;

public class QuickSort
{
    private int[] array;

    public void sort(int[] array)
    {
        this.array = array;
        sort(0, array.length - 1);
    }

    private void sort(int p, int r)
    {
        if (p < r)
        {
            int pivot = partition(p, r);

            sort(p, pivot - 1);
            sort(pivot + 1, r);
        }
    }

    private int partition(int p, int r)
    {
        int q = p;

        for (int j = p; j < r; j++)
        {
            if (array[j] <= array[r])
            {
                swap(q, j);
                q++;
            }
        }
        swap(q, r);

        return q;
    }

    private void swap(int firstIndex, int secondIndex)
    {
        int tmp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = tmp;
    }
}
