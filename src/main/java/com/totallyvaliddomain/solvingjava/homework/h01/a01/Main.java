package com.totallyvaliddomain.solvingjava.homework.h01.a01;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Main
{
    static
    {
        System.setProperty("log4j.configurationFile", "src/main/resources/log4j2.xml");
    }

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args)
    {
        QuickSort quickSort = new QuickSort();

        int[] array = {9, 7, 5, 11, 12, 2, 14, 3, 0, 6};
        LOGGER.info(Arrays.toString(array));
        quickSort.sort(array);
        LOGGER.info(Arrays.toString(array));

        array = new int[]{-29, 7, 5, 11, 12, -2, 14, 3, 0, 6};
        LOGGER.info(Arrays.toString(array));
        quickSort.sort(array);
        LOGGER.info(Arrays.toString(array));

        array = new int[]{1};
        LOGGER.info(Arrays.toString(array));
        quickSort.sort(array);
        LOGGER.info(Arrays.toString(array));

        array = new int[]{-2, 5};
        LOGGER.info(Arrays.toString(array));
        quickSort.sort(array);
        LOGGER.info(Arrays.toString(array));

        array = new int[]{5, -2};
        LOGGER.info(Arrays.toString(array));
        quickSort.sort(array);
        LOGGER.info(Arrays.toString(array));
    }
}
