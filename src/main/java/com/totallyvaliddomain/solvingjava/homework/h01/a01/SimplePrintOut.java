package com.totallyvaliddomain.solvingjava.homework.h01.a01;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SimplePrintOut
{
    private static final Logger LOGGER = LogManager.getLogger(SimplePrintOut.class);
    
    public static void main(String[] args)
    {
        for (String info : args)
        {
            LOGGER.info(info);
        }
    }
}


