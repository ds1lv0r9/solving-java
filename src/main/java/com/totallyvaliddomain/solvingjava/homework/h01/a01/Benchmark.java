package com.totallyvaliddomain.solvingjava.homework.h01.a01;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class Benchmark
{
    private static final Logger LOGGER = LogManager.getLogger(Benchmark.class);

    public static void main(String[] args)
    {
        Benchmark b = new Benchmark();
        b.run();
    }

    public void run()
    {
        int[] array;

        long startTime;
        long endTime;
        long totalTime;

        startTime = System.nanoTime();
        array = generateArray(10000000, -1000000, 1000000);
        endTime = System.nanoTime();
        totalTime = endTime - startTime;
        LOGGER.info(totalTime);

        QuickSort quickSort = new QuickSort();

        startTime = System.nanoTime();
        quickSort.sort(array);
        endTime = System.nanoTime();
        totalTime = endTime - startTime;
        LOGGER.info(totalTime);

        if (validateSort(array))
        {
            LOGGER.info("Array is sorted.");
        }
    }

    private int[] generateArray(int size, int minValue, int maxValue)
    {
        int[] array = new int[size];
        Random random = new Random();

        for (int i = 0; i < size; i++)
        {
            array[i] = random.nextInt(minValue, maxValue + 1);
        }

        return array;
    }

    public boolean validateSort(int[] array)
    {
        boolean isSortingValid = true;

        for (int i = 0; i < array.length - 1; i++)
        {
            if (array[i] > array[i + 1])
            {
                LOGGER.info("[" + i + "," + (i + 1) + ": " + array[i] + ">" + array[i + 1] + "] ");
                isSortingValid = false;
            }
        }

        return isSortingValid;
    }
}
