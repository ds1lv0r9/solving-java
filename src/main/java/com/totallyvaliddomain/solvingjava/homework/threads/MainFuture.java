package com.totallyvaliddomain.solvingjava.homework.threads;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.CompletableFuture;

public class MainFuture
{
    private static final Logger LOGGER = LogManager.getLogger(MainFuture.class.getSimpleName());

    public static void main(String[] args)
    {
        ConnectionPool connectionPool = ConnectionPool.getConnectionPool(5);

        LOGGER.info("Connection Pool - CompletableFuture");
        CompletableFuture<Void>[] completableFutures = new CompletableFuture[7];
        for (int i = 0; i < completableFutures.length; i++)
        {
            completableFutures[i] = CompletableFuture.runAsync(new PoolThread(connectionPool));
        }
        CompletableFuture<Void> allOfCompletableFuture = CompletableFuture.allOf(completableFutures);
        allOfCompletableFuture.join();
    }
}
