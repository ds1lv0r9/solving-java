package com.totallyvaliddomain.solvingjava.homework.threads;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConnectionPool
{
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class.getSimpleName());
    private volatile static ConnectionPool instance;
    private final BlockingQueue<Connection> connectionPool;
    private final BlockingQueue<Connection> usedConnectionPool;

    private int poolSize;

    private ConnectionPool(int poolSize)
    {
        connectionPool = new ArrayBlockingQueue<>(poolSize);
        usedConnectionPool = new ArrayBlockingQueue<>(poolSize);
        this.poolSize = poolSize;
    }

    public synchronized static ConnectionPool getConnectionPool(int poolSize)
    {
        if (instance == null)
        {
            instance = new ConnectionPool(poolSize);
        }

        return instance;
    }

    public synchronized Connection getConnection() throws InterruptedException
    {
        while (connectionPool.isEmpty())
        {
            if (size() < poolSize)
            {
                Connection connection = new Connection();
                usedConnectionPool.add(connection);
                LOGGER.info(this);
                return connection;
            }
            wait(1000);
        }

        Connection connection = connectionPool.remove();
        usedConnectionPool.add(connection);
        LOGGER.info(this);
        return connection;
    }

    public synchronized void releaseConnection(Connection connection)
    {
        connectionPool.add(connection);
        usedConnectionPool.remove(connection);
        LOGGER.info(this);
        notifyAll();
    }

    private int size()
    {
        return connectionPool.size() + usedConnectionPool.size();
    }

    @Override
    public String toString()
    {
        return "usedConnectionPool=" + usedConnectionPool + ", un/used: " + connectionPool.size() + "/" + usedConnectionPool.size();
    }
}
