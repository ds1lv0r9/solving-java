package com.totallyvaliddomain.solvingjava.homework.threads;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ThreadRunnable implements Runnable
{
    private static final Logger LOGGER = LogManager.getLogger(ThreadRunnable.class.getSimpleName());

    @Override
    public void run()
    {
        LOGGER.info("I implement Runnable.");
    }
}
