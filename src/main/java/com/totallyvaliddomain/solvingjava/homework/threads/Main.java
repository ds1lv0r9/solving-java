package com.totallyvaliddomain.solvingjava.homework.threads;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main
{
    private static final Logger LOGGER = LogManager.getLogger(Main.class.getSimpleName());

    public static void main(String[] args)
    {
        LOGGER.info("Threads");
        new Thread(new ThreadRunnable()).start();
        new ThreadThread().start();

        new Thread(() -> LOGGER.info("I implement Runnable too")).start();

        ConnectionPool connectionPool = ConnectionPool.getConnectionPool(5);
        LOGGER.info("Connection Pool 1");
        PoolThread[] threads = new PoolThread[7];
        for (int i = 0; i < threads.length; i++)
        {
            threads[i] = new PoolThread(connectionPool);
            threads[i].start();
        }
    }
}
