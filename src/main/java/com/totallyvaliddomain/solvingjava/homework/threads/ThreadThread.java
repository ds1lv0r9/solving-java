package com.totallyvaliddomain.solvingjava.homework.threads;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ThreadThread extends Thread
{
    private static final Logger LOGGER = LogManager.getLogger(ThreadThread.class.getSimpleName());

    @Override
    public void run()
    {
        LOGGER.info("I extend Thread.");
    }
}
