package com.totallyvaliddomain.solvingjava.homework.threads;

public class Connection
{
    @Override
    public String toString()
    {
        return "[" + Integer.toHexString(hashCode()) + "]";
    }
}
