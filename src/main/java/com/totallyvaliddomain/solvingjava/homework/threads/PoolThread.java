package com.totallyvaliddomain.solvingjava.homework.threads;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class PoolThread extends Thread
{
    private static final Logger LOGGER = LogManager.getLogger(PoolThread.class.getSimpleName());

    private Connection connection;
    private volatile ConnectionPool connectionPool;

    public PoolThread(ConnectionPool connectionPool)
    {
        this.connectionPool = connectionPool;
    }

    @Override
    public void run()
    {
        LOGGER.info(this + " started");
        try
        {
            acquireConnection();
            sleep(new Random().nextInt(1000, 2000));
            releaseConnection();
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void acquireConnection() throws InterruptedException
    {
        connection = connectionPool.getConnection();
        LOGGER.info(this + ": connection " + connection + " acquired");
    }

    public void releaseConnection() throws InterruptedException
    {
        connectionPool.releaseConnection(connection);
        LOGGER.info(this + ": connection " + connection + " released");
    }

    @Override
    public String toString()
    {
        return "Thread [" + Integer.toHexString(hashCode()) + "]";
    }
}
