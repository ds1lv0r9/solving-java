package com.totallyvaliddomain.solvingjava.homework.oop.exceptions;

public class InvalidDoorInstallTimeException extends RuntimeException
{
    @Override
    public String getMessage()
    {
        return "Invalid door installation time.";
    }
}
