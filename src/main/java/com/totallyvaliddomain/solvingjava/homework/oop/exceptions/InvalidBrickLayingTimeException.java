package com.totallyvaliddomain.solvingjava.homework.oop.exceptions;

public class InvalidBrickLayingTimeException extends RuntimeException
{
    @Override
    public String getMessage()
    {
        return "Invalid brick laying time.";
    }
}
