package com.totallyvaliddomain.solvingjava.homework.oop.interfaces;

public interface Addressable
{
    public String getAddress();
    public void setAddress(String address);
}
