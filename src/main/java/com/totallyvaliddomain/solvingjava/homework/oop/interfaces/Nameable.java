package com.totallyvaliddomain.solvingjava.homework.oop.interfaces;

public interface Nameable
{
    public String getName();
    public void setName(String name);
}
