package com.totallyvaliddomain.solvingjava.homework.oop.enums;

public enum SearchConstraints
{
    PRICE("Search by price"),
    TIME("Search by construction time"),
    TYPE("Search by building type");

    private final String description;

    SearchConstraints(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }
}
