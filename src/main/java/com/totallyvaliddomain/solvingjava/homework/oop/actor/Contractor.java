package com.totallyvaliddomain.solvingjava.homework.oop.actor;

import com.totallyvaliddomain.solvingjava.homework.oop.building.Building;
import com.totallyvaliddomain.solvingjava.homework.oop.interfaces.Nameable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class Contractor implements Nameable
{
    private String name;
    private Collection<Building> buildingOffers;

    public Contractor(String name)
    {
        this.name = name;
        buildingOffers = new ArrayList<>();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Collection<Building> getBuildingOffers()
    {
        return buildingOffers;
    }

    public void setBuildingOffers(Collection<Building> buildingOffers)
    {
        this.buildingOffers = buildingOffers;
    }

    public void addOffer(Building building)
    {
        buildingOffers.add(building);
    }

    @Override
    public String toString()
    {
        String buildingsOnOffer = buildingOffers.size() > 0 ? "" : " nothing";

        for (Building b : buildingOffers)
        {
            buildingsOnOffer += " " + b.getType() + "s";
        }

        return name + " offers" + buildingsOnOffer;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contractor contractor = (Contractor) o;
        return Objects.equals(name, contractor.name) && Objects.equals(buildingOffers, contractor.getBuildingOffers());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name, buildingOffers);
    }
}
