package com.totallyvaliddomain.solvingjava.homework.oop.enums;

public enum BuildingType
{
    BUNGALOW("Bungalow"),
    COTTAGE("Cottage"),
    MANSION("Mansion"),
    LOW_RISE("Low Rise"),
    MID_RISE("Mid Rise"),
    HIGH_RISE("High Rise"),
    PRESIDENTIAL_PALACE("Presidential Palace");

    private final String name;

    BuildingType(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
