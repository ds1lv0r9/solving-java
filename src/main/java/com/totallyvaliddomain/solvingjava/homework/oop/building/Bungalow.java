package com.totallyvaliddomain.solvingjava.homework.oop.building;

import com.totallyvaliddomain.solvingjava.homework.oop.enums.BuildingType;

public class Bungalow extends Building
{
    public Bungalow(int brickLayingTime, int foundationCuringTime, int windowInstallTime, int doorInstallTime, int windowCount, int windowPrice, int doorCount, int doorPrice, int brickCount, int brickPrice, int foundationCuringCost)
    {
        super(brickLayingTime, foundationCuringTime, windowInstallTime, doorInstallTime, BuildingType.BUNGALOW, windowCount, windowPrice, doorCount, doorPrice, brickCount, brickPrice, foundationCuringCost);
    }

    @Override
    public String getBuildingInformation()
    {
        return "A spacious " + getType() + "\n" + "It's construction time is " + calculateConstructionTime() + " months.\nIt will cost " + calculateConstructionCost() + " to build";
    }
}


