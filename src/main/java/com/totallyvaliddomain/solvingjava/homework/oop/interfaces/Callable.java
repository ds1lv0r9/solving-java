package com.totallyvaliddomain.solvingjava.homework.oop.interfaces;

public interface Callable
{
    public String getPhoneNumber();
    public void setPhoneNumber(String phoneNumber);
}
