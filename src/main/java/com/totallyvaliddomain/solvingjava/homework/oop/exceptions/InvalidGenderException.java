package com.totallyvaliddomain.solvingjava.homework.oop.exceptions;

public class InvalidGenderException extends Exception
{
    @Override
    public String getMessage()
    {
        return "Invalid gender exception";
    }
}
