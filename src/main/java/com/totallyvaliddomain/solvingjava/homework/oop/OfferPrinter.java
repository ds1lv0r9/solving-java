package com.totallyvaliddomain.solvingjava.homework.oop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class OfferPrinter
{
    private static final Logger LOGGER = LogManager.getLogger(OfferPrinter.class.getSimpleName());

    public static void print(String[] offers)
    {
        for (String offer : offers)
        {
            LOGGER.info(offer);
        }
    }
}
