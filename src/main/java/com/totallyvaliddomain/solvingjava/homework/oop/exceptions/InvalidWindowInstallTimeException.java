package com.totallyvaliddomain.solvingjava.homework.oop.exceptions;

public class InvalidWindowInstallTimeException extends RuntimeException
{
    @Override
    public String getMessage()
    {
        return "Invalid window installation time.";
    }
}
