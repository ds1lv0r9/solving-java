package com.totallyvaliddomain.solvingjava.homework.oop.actor;

import com.totallyvaliddomain.solvingjava.homework.oop.building.Building;
import com.totallyvaliddomain.solvingjava.homework.oop.building.DesiredBuilding;
import com.totallyvaliddomain.solvingjava.homework.oop.enums.SearchConstraints;
import com.totallyvaliddomain.solvingjava.homework.oop.interfaces.Nameable;
import com.totallyvaliddomain.solvingjava.homework.oop.interfaces.Typeable;
import com.totallyvaliddomain.solvingjava.homework.oop.linkedlist.LinkedList;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

final public class Broker implements Nameable
{
    private String name;
    private Collection<Contractor> contractors;

    public Broker(String name)
    {
        this.name = name;
        this.contractors = new ArrayList<>();
    }

    public String getName()
    {
        return name;
    }

    public String getQuoteOfTheDay(String path) throws IOException
    {
        LinkedList<String> quotes = new LinkedList<>();

        try (FileReader fr = new FileReader(path);
             BufferedReader br = new BufferedReader(fr))
        {
            quotes.add(br.readLine());
        }

        return quotes.getFirst();
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Collection<Contractor> getContractors()
    {
        return contractors;
    }

    public void setContractors(Collection<Contractor> contractors)
    {
        this.contractors = contractors;
    }

    public void addContractor(Contractor contractor)
    {
        contractors.add(contractor);
    }

    public String[] findOffers(Map<SearchConstraints, Boolean> constraints, DesiredBuilding desiredBuilding)
    {
        boolean constraintsExist = false;
        for (boolean constraint : constraints.values())
        {
            constraintsExist = constraintsExist || constraint;
        }
        if (!constraintsExist)
        {
            return new String[0];
        }

        if (desiredBuilding == null)
        {
            return new String[0];
        }

        Collection<String> offers = new ArrayList<>();

        for (Contractor contractor : contractors)
        {
            Collection<Building> contractorOffers = contractor.getBuildingOffers();
            for (Building buildingOffer : contractorOffers)
            {
                if (constraints.get(SearchConstraints.PRICE) != null && constraints.get(SearchConstraints.PRICE)
                        && buildingOffer.calculateConstructionCost() > desiredBuilding.getPrice())
                {
                    continue;
                }
                if (constraints.get(SearchConstraints.TIME) != null && constraints.get(SearchConstraints.TIME)
                        && buildingOffer.calculateConstructionTime() / 30 > desiredBuilding.getBuildTime())
                {
                    continue;
                }
                if (constraints.get(SearchConstraints.TYPE) != null && constraints.get(SearchConstraints.TYPE)
                        && (buildingOffer.getType().compareTo(desiredBuilding.getType()) != 0))
                {
                    continue;
                }

                offers.add(contractor.getName() + " will build a " + buildingOffer.getType() + " for " + buildingOffer.calculateConstructionCost() + " in " + buildingOffer.calculateConstructionTime() / 30 + " months");
            }
        }

        return offers.toArray(new String[0]);
    }

    public String[] findOffersByContractor(Map<SearchConstraints, Boolean> constraints, DesiredBuilding desiredBuilding, Predicate<Contractor> selectedContractor)
    {
        boolean constraintsExist = false;
        for (boolean constraint : constraints.values())
        {
            constraintsExist = constraintsExist || constraint;
        }
        if (!constraintsExist)
        {
            return new String[0];
        }

        if (desiredBuilding == null)
        {
            return new String[0];
        }

        Collection<String> offers = new ArrayList<>();

        for (Contractor contractor : contractors)
        {
            if (selectedContractor.test(contractor))
            {
                Collection<Building> contractorOffers = contractor.getBuildingOffers();
                for (Building buildingOffer : contractorOffers)
                {
                    if (constraints.get(SearchConstraints.PRICE) != null && constraints.get(SearchConstraints.PRICE)
                            && buildingOffer.calculateConstructionCost() > desiredBuilding.getPrice())
                    {
                        continue;
                    }
                    if (constraints.get(SearchConstraints.TIME) != null && constraints.get(SearchConstraints.TIME)
                            && buildingOffer.calculateConstructionTime() / 30 > desiredBuilding.getBuildTime())
                    {
                        continue;
                    }
                    if (constraints.get(SearchConstraints.TYPE) != null && constraints.get(SearchConstraints.TYPE)
                            && (buildingOffer.getType().compareTo(desiredBuilding.getType()) != 0))
                    {
                        continue;
                    }

                    offers.add(contractor.getName() + " will build a " + buildingOffer.getType() + " for " + buildingOffer.calculateConstructionCost() + " in " + buildingOffer.calculateConstructionTime() / 30 + " months");
                }
            }
        }

        return offers.toArray(new String[0]);
    }

    public String[] listOffers()
    {
        Collection<String> offers = new ArrayList<>();

        for (Contractor contractor : contractors)
        {
            Collection<Building> contractorOffers = contractor.getBuildingOffers();
            for (Building buildingOffer : contractorOffers)
            {
                offers.add(contractor.getName() + " will build a " + buildingOffer.getType() + " for " + buildingOffer.calculateConstructionCost() + " in " + buildingOffer.calculateConstructionTime() / 30 + " months");
            }
        }

        return offers.toArray(new String[0]);
    }

    @Override
    public String toString()
    {
        return name + " - find the best offers on this side of the compiler!";
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Broker broker = (Broker) o;
        return Objects.equals(name, broker.name) && Objects.equals(contractors, broker.contractors);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name, contractors);
    }

    public static String getType(Typeable typeable)
    {
        return typeable.getType();
    }
}
