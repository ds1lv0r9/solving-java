package com.totallyvaliddomain.solvingjava.homework.oop.interfaces;

public interface Typeable
{
    public String getType();
}
