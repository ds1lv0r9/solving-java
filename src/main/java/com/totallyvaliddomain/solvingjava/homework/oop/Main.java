package com.totallyvaliddomain.solvingjava.homework.oop;

import com.totallyvaliddomain.solvingjava.homework.oop.actor.Broker;
import com.totallyvaliddomain.solvingjava.homework.oop.actor.Client;
import com.totallyvaliddomain.solvingjava.homework.oop.actor.Contractor;
import com.totallyvaliddomain.solvingjava.homework.oop.building.*;
import com.totallyvaliddomain.solvingjava.homework.oop.enums.SearchConstraints;
import com.totallyvaliddomain.solvingjava.homework.oop.exceptions.InvalidGenderException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Main
{
    private static final Logger LOGGER = LogManager.getLogger(Main.class.getSimpleName());

    public static void main(String[] args)
    {
        Contractor buildRUs = new Contractor("Build'r'us");
        Contractor speedyBuilders = new Contractor("Speedy Builders");
        Contractor leRoyal = new Contractor("le Royal");
        Building bungalow = null;

        buildRUs.addOffer(new LowRise(10000, 7 * 60 * 24, 90, 150, 25, 1000, 20, 2500, 10000, 20, 5000));
        buildRUs.addOffer(new MidRise(10000, 7 * 60 * 24, 90, 150, 50, 1000, 40, 2500, 20000, 20, 10000));
        buildRUs.addOffer(new HighRise(10000, 7 * 60 * 24, 90, 150, 75, 1000, 60, 2500, 35000, 20, 15000));

        speedyBuilders.addOffer(new Bungalow(8000, 6 * 60 * 24, 60, 100, 5, 1000, 2, 2500, 5000, 20, 10000));
        speedyBuilders.addOffer(new Cottage(8000, 6 * 60 * 24, 60, 100, 5, 1000, 2, 2500, 5000, 20, 10000));
        speedyBuilders.addOffer(new LowRise(8000, 6 * 60 * 24, 60, 100, 25, 1000, 20, 2500, 10000, 20, 1000));

        leRoyal.addOffer(new Mansion(10000, 7 * 60 * 24, 90, 150, 5, 1000, 2, 2500, 5000, 20, 5000));
        leRoyal.addOffer(new PresidentialPalace(10000, 7 * 60 * 24, 90, 150, 150, 1000, 20, 2500, 25000, 20, 5000));

        bungalow = new Bungalow(8000, 6 * 60 * 24, 60, 100, 5, 1000, 2, 2500, 5000, 20, 10000);

        LOGGER.info("Building information:");
        LOGGER.info(bungalow.getBuildingInformation());
        LOGGER.info("Building type:");
        LOGGER.info(Broker.getType(bungalow));

        Broker broker = new Broker("WeBroke");
        broker.addContractor(buildRUs);
        broker.addContractor(speedyBuilders);
        broker.addContractor(leRoyal);

        LOGGER.info("Quote of the day:");
        try
        {
            LOGGER.info(broker.getQuoteOfTheDay("src/main/resources/quotes"));
        }
        catch (IOException e)
        {
            LOGGER.error(e.getMessage());
        }

        Client johnSnow = new Client("John Snow", new DesiredBuilding("PresidentialPalace", 1050000, 50));
        Client aryaStark = new Client("Arya Stark", new DesiredBuilding("Cottage", 2000000, 15));

        try
        {
            johnSnow.setGender("malee");
        }
        catch (InvalidGenderException e)
        {
            LOGGER.error(e.getMessage());
        }

        LOGGER.info("LIST ALL OFFERS");
        OfferPrinter.print(broker.listOffers());
        LOGGER.info("");

        Map<SearchConstraints, Boolean> searchConstraints = new HashMap<>();

        LOGGER.info("CLIENT " + johnSnow.getName() + " LOOKING FOR AN OFFER");
        searchConstraints.put(SearchConstraints.PRICE, true);
        searchConstraints.put(SearchConstraints.TIME, true);
        searchConstraints.put(SearchConstraints.TYPE, false);
        OfferPrinter.print(broker.findOffers(searchConstraints, johnSnow.getDesiredBuilding()));
        LOGGER.info("");

        LOGGER.info("CLIENT " + aryaStark.getName() + " LOOKING FOR AN OFFER");
        searchConstraints.put(SearchConstraints.PRICE, true);
        searchConstraints.put(SearchConstraints.TIME, true);
        searchConstraints.put(SearchConstraints.TYPE, true);
        OfferPrinter.print(broker.findOffers(searchConstraints, aryaStark.getDesiredBuilding()));


        LOGGER.info("CLIENT " + johnSnow.getName() + " LOOKING FOR AN OFFER");
        searchConstraints.put(SearchConstraints.PRICE, true);
        searchConstraints.put(SearchConstraints.TIME, true);
        searchConstraints.put(SearchConstraints.TYPE, false);
        LOGGER.info("offers from buildRUs");
        OfferPrinter.print(broker.findOffersByContractor(searchConstraints, johnSnow.getDesiredBuilding(), (contractor) -> contractor.equals(buildRUs)));
        LOGGER.info("offers from speedyBuilders");
        OfferPrinter.print(broker.findOffersByContractor(searchConstraints, johnSnow.getDesiredBuilding(), (contractor) -> contractor.equals(speedyBuilders)));
        LOGGER.info("");

        LOGGER.info("STREAMS");
        LOGGER.info("find offers for a building type from a builder");
        Collection<Building> list = speedyBuilders.getBuildingOffers().stream()
                .filter(b -> b.getType().equals("Bungalow"))
                .toList();
        for (Building offer : list)
        {
            LOGGER.info(offer.getBuildingInformation());
        }

        LOGGER.info("find offers for a building type from all builders");
        Collection<Building> offers = broker.getContractors().stream()
                .flatMap(contractor -> contractor.getBuildingOffers().stream())
                .filter(building -> building.getType().equals("Low Rise"))
                .peek(building -> LOGGER.info(building.getBuildingInformation()))
                .toList();
    }
}
