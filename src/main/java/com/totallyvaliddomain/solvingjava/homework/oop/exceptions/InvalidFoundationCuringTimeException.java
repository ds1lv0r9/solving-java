package com.totallyvaliddomain.solvingjava.homework.oop.exceptions;

public class InvalidFoundationCuringTimeException extends RuntimeException
{
    @Override
    public String getMessage()
    {
        return "Invalid foundation curing time.";
    }
}
