package com.totallyvaliddomain.solvingjava.homework.oop.building;

import com.totallyvaliddomain.solvingjava.homework.oop.enums.BuildingType;
import com.totallyvaliddomain.solvingjava.homework.oop.exceptions.InvalidBrickLayingTimeException;
import com.totallyvaliddomain.solvingjava.homework.oop.exceptions.InvalidDoorInstallTimeException;
import com.totallyvaliddomain.solvingjava.homework.oop.exceptions.InvalidFoundationCuringTimeException;
import com.totallyvaliddomain.solvingjava.homework.oop.exceptions.InvalidWindowInstallTimeException;
import com.totallyvaliddomain.solvingjava.homework.oop.interfaces.Typeable;

import java.util.Objects;

public abstract class Building implements Typeable
{
    private final int BRICK_LAYING_TIME;
    private final int FOUNDATION_CURING_TIME;
    private final int WINDOW_INSTALL_TIME;
    private final int DOOR_INSTALL_TIME;
    private final BuildingType type;
    protected int windowCount;
    protected int windowPrice;
    protected int doorCount;
    protected int doorPrice;
    protected int brickCount;
    protected int brickPrice;
    protected int foundationCuringCost;

    public Building(int brickLayingTime, int foundationCuringTime, int windowInstallTime, int doorInstallTime, BuildingType type, int windowCount, int windowPrice, int doorCount, int doorPrice, int brickCount, int brickPrice, int foundationCuringCost)
    {
        if (brickLayingTime <= 0) throw new InvalidBrickLayingTimeException();
        this.BRICK_LAYING_TIME = brickLayingTime;
        if (foundationCuringTime <= 0) throw new InvalidFoundationCuringTimeException();
        this.FOUNDATION_CURING_TIME = foundationCuringTime;
        if (windowInstallTime <= 0) throw new InvalidWindowInstallTimeException();
        this.WINDOW_INSTALL_TIME = windowInstallTime;
        if (doorInstallTime <= 0) throw new InvalidDoorInstallTimeException();
        this.DOOR_INSTALL_TIME = doorInstallTime;
        this.type = type;
        this.windowCount = windowCount;
        this.windowPrice = windowPrice;
        this.doorCount = doorCount;
        this.doorPrice = doorPrice;
        this.brickCount = brickCount;
        this.brickPrice = brickPrice;
        this.foundationCuringCost = foundationCuringCost;
    }

    public String getType()
    {
        return type.getName();
    }

    abstract public String getBuildingInformation();

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Building building = (Building) o;
        return BRICK_LAYING_TIME == building.BRICK_LAYING_TIME && FOUNDATION_CURING_TIME == building.FOUNDATION_CURING_TIME && WINDOW_INSTALL_TIME == building.WINDOW_INSTALL_TIME && DOOR_INSTALL_TIME == building.DOOR_INSTALL_TIME && windowCount == building.windowCount && windowPrice == building.windowPrice && doorCount == building.doorCount && doorPrice == building.doorPrice && brickCount == building.brickCount && brickPrice == building.brickPrice && foundationCuringCost == building.foundationCuringCost && Objects.equals(type, building.type);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(BRICK_LAYING_TIME, FOUNDATION_CURING_TIME, WINDOW_INSTALL_TIME, DOOR_INSTALL_TIME, type, windowCount, windowPrice, doorCount, doorPrice, brickCount, brickPrice, foundationCuringCost);
    }

    public final int calculateConstructionCost()
    {
        return foundationCuringCost + brickCount * brickPrice + windowCount * windowPrice + doorCount * doorPrice;
    }

    /**
     * @return construction time in days
     */
    public int calculateConstructionTime()
    {
        return (FOUNDATION_CURING_TIME + brickCount / 100 * BRICK_LAYING_TIME + windowCount * WINDOW_INSTALL_TIME + doorCount * DOOR_INSTALL_TIME) / 60 / 24;
    }
}
