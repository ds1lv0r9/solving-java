package com.totallyvaliddomain.solvingjava.homework.oop.linkedlist;

import java.util.NoSuchElementException;

public class LinkedList<E>
{
    Node<E> first;
    Node<E> last;
    int size = 0;

    public LinkedList()
    {
    }

    public int size()
    {
        return size;
    }

    public E getFirst()
    {
        Node<E> first = this.first;
        if (first == null) throw new NoSuchElementException();
        return first.getElement();
    }

    public E getLast()
    {
        Node<E> last = this.last;
        if (last == null) throw new NoSuchElementException();
        return last.getElement();
    }

    public boolean add(E element)
    {
        Node<E> last = this.last;
        Node<E> node = new Node<>(element, last, null);
        this.last = node;
        if (last == null) first = node;
        else last.setNext(node);
        size++;
        return true;
    }

    public void addFirst(E element)
    {
        Node<E> first = this.first;
        Node<E> node = new Node<>(element, null, first);
        this.first = node;

        if (first == null) last = node;
        else first.setPrevious(node);
        size++;
    }

    public void addLast(E element)
    {
        add(element);
    }

    public E removeFirst()
    {
        Node<E> first = this.first;
        if (first == null) throw new NoSuchElementException();

        unlinkNode(first);

        return first.getElement();
    }

    private void unlinkNode(Node<E> node)
    {
        if (node == null) throw new NoSuchElementException();

        Node<E> previousNode = node.getPrevious();
        Node<E> nextNode = node.getNext();

        if (previousNode == nextNode)
        {
            first = null;
            last = null;
        }

        if (nextNode != null)
        {
            nextNode.setPrevious(node.getPrevious());
            if (nextNode.getPrevious() == null)
                first = nextNode;
        }
        if (previousNode != null)
        {
            previousNode.setNext(node.getNext());
            if (previousNode.getNext() == null)
                last = previousNode;
        }

        size--;
    }

    public E removeLast()
    {
        Node<E> last = this.last;
        if (last == null) throw new NoSuchElementException();

        Node<E> preLast = last.getPrevious();
        if (preLast == null)
            this.last = null;
        else
        {
            this.last = preLast;
            last.setNext(null);
        }

        return last.getElement();
    }

    public E push(E element)
    {
        addFirst(element);
        return getFirst();
    }

    public E pop()
    {
        return removeFirst();
    }

    public E remove()
    {
        return removeFirst();
    }

    public E remove(int index)
    {
        if (index < 0 || index >= size) throw new IndexOutOfBoundsException();

        Node<E> node;

        if (index < size / 2)
        {
            node = first;
            for (int i = 0; i < index; i++)
            {
                node = node.getNext();
            }
        }
        else
        {
            node = last;
            for (int i = size - 1; i > index; i--)
            {
                node = node.getPrevious();
            }
        }

        unlinkNode(node);

        return node.getElement();
    }

    public boolean remove(Object o)
    {
        Node<E> node = first;

        while (node != null)
        {
            if (node.getElement() == null || node.getElement().equals(o))
            {
                unlinkNode(node);
                return true;
            }

            node = node.getNext();
        }

        return false;
    }
}