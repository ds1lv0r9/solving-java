package com.totallyvaliddomain.solvingjava.homework.oop.interfaces;

public interface Ageable
{
    public int getAge();
    public void setAge(int age);
}
