package com.totallyvaliddomain.solvingjava.homework.oop.actor;

import com.totallyvaliddomain.solvingjava.homework.oop.building.DesiredBuilding;
import com.totallyvaliddomain.solvingjava.homework.oop.exceptions.InvalidGenderException;
import com.totallyvaliddomain.solvingjava.homework.oop.interfaces.Addressable;
import com.totallyvaliddomain.solvingjava.homework.oop.interfaces.Ageable;
import com.totallyvaliddomain.solvingjava.homework.oop.interfaces.Callable;
import com.totallyvaliddomain.solvingjava.homework.oop.interfaces.Nameable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

public class Client implements Addressable, Ageable, Callable, Nameable
{
    private static final Logger LOGGER = LogManager.getLogger(Client.class.getSimpleName());
    private String name;
    private DesiredBuilding desiredBuilding;
    private String address;
    private int age;
    private String phoneNumber;
    public static String description = "A client looking to buy a building.";
    private String gender;

    static
    {
        LOGGER.info(description);
    }

    public Client(String name, DesiredBuilding desiredBuilding)
    {
        this.name = name;
        this.desiredBuilding = desiredBuilding;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public DesiredBuilding getDesiredBuilding()
    {
        return desiredBuilding;
    }

    public void setDesiredBuilding(DesiredBuilding desiredBuilding)
    {
        this.desiredBuilding = desiredBuilding;
    }

    @Override
    public String toString()
    {
        return "Hi, my name is " + name + ". I'm looking to buy a " + desiredBuilding.getType() + ".";
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(name, client.name) && Objects.equals(desiredBuilding, client.desiredBuilding);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name, desiredBuilding);
    }

    @Override
    public String getAddress()
    {
        return address;
    }

    @Override
    public void setAddress(String address)
    {
        this.address = address;
    }

    @Override
    public int getAge()
    {
        return age;
    }

    @Override
    public void setAge(int age)
    {
        this.age = age;
    }

    @Override
    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    @Override
    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getGender()
    {
        return gender;
    }

    public void setGender(String gender) throws InvalidGenderException
    {
        gender = gender.toLowerCase();
        if (!(gender.equals("male") || gender.equals("female")))
        {
            throw new InvalidGenderException();
        }

        this.gender = gender;
    }
}
