package com.totallyvaliddomain.solvingjava.homework.oop.building;

public class DesiredBuilding
{
    private String type;
    private int price;
    private int buildTime;

    public DesiredBuilding(String type, int price, int buildTime)
    {
        this.type = type;
        this.price = price;
        this.buildTime = buildTime;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public int getBuildTime()
    {
        return buildTime;
    }

    public void setBuildTime(int buildTime)
    {
        this.buildTime = buildTime;
    }
}
