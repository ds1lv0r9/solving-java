package com.totallyvaliddomain.solvingjava.homework.oop.reflection;

import com.totallyvaliddomain.solvingjava.homework.oop.building.DesiredBuilding;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.*;

public class Main
{
    private static final Logger LOGGER = LogManager.getLogger(Main.class.getSimpleName());

    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException
    {
        Class<?> desiredBuildingClass = Class.forName("com.totallyvaliddomain.solvingjava.homework.oop.building.DesiredBuilding");

        String name = desiredBuildingClass.getName();
        LOGGER.info("Class name: " + name);
        LOGGER.info("modifier(s): " + Modifier.toString(desiredBuildingClass.getModifiers()));

        Class<?> superClass = desiredBuildingClass.getSuperclass();
        LOGGER.info("superclass: " + superClass.getSimpleName());
        LOGGER.info("");

        Constructor<?>[] constructors = desiredBuildingClass.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors)
        {
            LOGGER.info("Constructor name: " + constructor.getName());
            LOGGER.info("modifier(s): " + Modifier.toString(constructor.getModifiers()));
            LOGGER.info("parameter(s): " + constructor.getParameterCount());
            Parameter[] parameters = constructor.getParameters();
            for (Parameter parameter : parameters)
            {
                LOGGER.info("\tparameter: " + parameter.getName() + ", type: " + parameter.getType());
            }
            LOGGER.info("");
        }

        Method[] methods = desiredBuildingClass.getDeclaredMethods();
        for (Method method : methods)
        {
            LOGGER.info("Method name: " + method.getName());
            LOGGER.info("modifier(s): " + Modifier.toString(method.getModifiers()));
            LOGGER.info("return type: " + method.getReturnType());
            Parameter[] parameters = method.getParameters();
            for (Parameter p : parameters)
            {
                LOGGER.info("\tparameter: " + p.getName() + ", type: " + p.getType());
            }
            LOGGER.info("");
        }

        Field[] fields = desiredBuildingClass.getDeclaredFields();
        for (Field field : fields)
        {
            LOGGER.info("Field name: " + field.getName());
            LOGGER.info("modifier(s): " + Modifier.toString(field.getModifiers()));
            LOGGER.info("");
        }

        LOGGER.info("create object");
        DesiredBuilding desiredBuilding = DesiredBuilding.class.getConstructor(String.class, int.class, int.class).newInstance("TEST", 1, 2);
        LOGGER.info(desiredBuilding.getType() + " " + desiredBuilding.getPrice() + " " + desiredBuilding.getBuildTime());

        LOGGER.info("invoke methods");
        Method setBuildTime = DesiredBuilding.class.getMethod("setBuildTime", int.class);
        setBuildTime.invoke(desiredBuilding, 42);
        Method getBuildTime = DesiredBuilding.class.getMethod("getBuildTime");
        LOGGER.info(getBuildTime.invoke(desiredBuilding));
    }
}
