package com.totallyvaliddomain.solvingjava.homework.h08;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class Main
{
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException
    {
        String sourceFile = "src/main/resources/h08";
        String resultFile = "src/main/resources/h08result";
        int uniqueWords = 0;
        String fileContent;

        fileContent = FileUtils.readFileToString(new File(sourceFile), "UTF-8");

        fileContent = StringUtils.lowerCase(fileContent);
        fileContent = RegExUtils.replaceAll(fileContent, "[\\.\\,\\-\\+\\:\\;\\(\\)\\[\\]0-9]", "");
        fileContent = StringUtils.normalizeSpace(fileContent);

        String[] words = StringUtils.split(fileContent);

        for (String tested : words)
        {
            int count = 0;
            for (String word : words)
                if (word.equals(tested))
                    count++;

            if (count == 1)
                uniqueWords++;
        }
        FileUtils.writeStringToFile(new File(resultFile), Integer.toString(uniqueWords), "UTF-8");
    }
}
